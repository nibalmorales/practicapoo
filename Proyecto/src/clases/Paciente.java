package clases;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import interfaces.CitaMedica;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import vistas.VistaPrincipal;

/*
 * Clase Hija de clase Paciente.
 * @author Aníbal Roberto Gómez Morales
 * @version 1.0
 * @since 2021-15-08
 */
public class Paciente extends Persona implements CitaMedica {

    //Almacena un Id unico del objeto Paciente
    private int idpaciente;
    //Almacena un numero telefonico del objeto Paciente
    private int telefono;
    //Almacena una direccion del objeto Paciente 
    private String direccion;
    //Almacena un estado del objeto Paciente 
    private String estado;

    //Brinda valores iniciales a las variables de instancia definidas por la clase Paciente.
    public Paciente(int idpaciente, String nombre, String fechaNacimiento, char sexo, int telefono, String direccion, String estado) {
        super(nombre, fechaNacimiento, sexo);
        this.idpaciente = idpaciente;
        this.telefono = telefono;
        this.direccion = direccion;
        this.estado = estado;
    }

    //Recuperar el valor de un atributo Idpaciente
    public int getIdpaciente() {
        return idpaciente;
    }

    //Asignar un valor al atributo idPaciente
    public void setIdpaciente(int idpaciente) {
        this.idpaciente = idpaciente;
    }

    //Recuperar el valor de un atributo Teléfono
    public int getTelefono() {
        return telefono;
    }

    //Asignar un valor al atributo Telefono
    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    //Recuperar el valor de un atributo Dirección
    public String getDireccion() {
        return direccion;
    }

    //Asignar un valor al atributo Direccion
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    //Recuperar el valor del atributo Estado
    public String getEstado() {
        return estado;
    }

    //Asignar un valor al atributo Estado
    public void setEstado(String estado) {
        this.estado = estado;
    }

    //* Sobre escritura del Metodo abstracto verDatos() de la Clase Padre Persona.
    @Override
    public void verDatos() {
        JOptionPane.showMessageDialog(null, "ID del Paciente: " + idpaciente
                + "\nNombre: " + nombre
                + "\nEdad: " + fechaNacimiento
                + "\nSexo: " + sexo
                + "\nTelefono: " + telefono
                + "\nDirección: " + direccion
                + "\nTipo de Persona: " + estado,
                "Inforación de la Persona", JOptionPane.INFORMATION_MESSAGE);

    }

    // Sobre escritura del Metodo vacunarPaciente de la Interfaz CitaMedica.
    @Override
    public void imprimirCita() {
        File path = new File(nombre + ".pdf");
        try {
            Desktop.getDesktop().open(path);
        } catch (IOException ex) {
            Logger.getLogger(Paciente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*
     * Sobre escritura del Metodo entregarRecetaMedica de la Interfaz
     * CitaMedica.
     */
    @Override
    public void generarReporteVacunacion(String descripcion) {

        try {

            Document documento = new Document();
            FileOutputStream ficheroPdf = new FileOutputStream(nombre + ".pdf");
            PdfWriter.getInstance(documento, ficheroPdf).setInitialLeading(20);
            documento.open();

            documento.add(new Paragraph("Control de Vacunación",
                    FontFactory.getFont("arial", // fuente
                            22, // tamaño
                            Font.BOLD, // estilo
                            BaseColor.BLACK)));
            documento.add(new Paragraph(Chunk.NEWLINE));
            documento.add(new Paragraph("No. Afiliación: " + idpaciente));
            documento.add(new Paragraph("Nombre del Paciente: " + nombre));
            String[] edadTotal = fechaNacimiento.split(",");
            String edad = edadTotal[0] + " " + edadTotal[1] + " " + edadTotal[2];

            documento.add(new Paragraph("Edad: " + edad));
            documento.add(new Paragraph("Teléfono: " + telefono));
            documento.add(new Paragraph("Dirección: " + direccion));
            documento.add(new Paragraph("Estado: " + estado));
            documento.add(new Paragraph(Chunk.NEWLINE));

            String cadena[] = descripcion.split(";");
            String parte1 = cadena[0];
            String parte2 = cadena[1];
            String parte3 = cadena[2];
            String parte4 = cadena[3];
            String parte5 = cadena[4];
            String parte6 = cadena[5];

            documento.add(new Paragraph("Información de la Vacunas",
                    FontFactory.getFont("arial", // fuente
                            14, // tamaño
                            Font.BOLD, // estilo
                            BaseColor.BLACK)));
            documento.add(new Paragraph(Chunk.NEWLINE));
            
            PdfPTable tabla = new PdfPTable(1);
            float[] medidaCeldas1 = {2.00f};
            tabla.setWidths(medidaCeldas1);
            tabla.setWidthPercentage(100);
            tabla.setSpacingBefore(0f);
            tabla.setSpacingAfter(0f);
            
            for(int i = 0; i<cadena.length;i++){

                if(cadena[i].toString().equals("e")){
                    tabla.addCell(new Paragraph(Chunk.NEWLINE));
                    tabla.addCell(new Paragraph(Chunk.NEWLINE));
                    
                }else{
                    tabla.addCell(cadena[i]);
                }
            }
            
            documento.add(tabla);
            documento.close();

        } catch (DocumentException ex) {
            Logger.getLogger(VistaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Paciente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    
    
    
    
}
