package clases;

import java.time.LocalDate;
import java.time.Period;

/**
 * Clase Persona
 * Una clase abstracta contiene almenos un metodo que no ha sido especificado,
 * es decir, este sera heredado para sobreescribirse. Con la abstraccion
 * protegemos a la clase para que no se pueda instanciar.
 *
 * @author Aníbal Roberto Gómez Morales
 * @version 1.0
 * @since 2021-15-08
 */
public abstract class Persona {

    //Almacena un Nombre del objeto Persona 
    public String nombre;
    //Almacena una fecha de nacimiento objeto Persona
    protected String fechaNacimiento;
    //Almacena el sexo del objeto Persona
    public char sexo;

    /**
     * Brinda valores iniciales a las variables de instancia definidas por la
     * clase Persona.
     */
    public Persona(String nombre, String fechaNacimiento, char sexo) {

        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.sexo = sexo;
    }

    //Recuperar el valor de un atributo Nombre
    public String getNombre() {
        return nombre;
    }

    //Asignar un valor al atributo Nombre 
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    //Recuperar el valor de un atributo FechaNacimiento
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    //Asignar un valor al atributo fechaNacimiento
    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = this.edad(fechaNacimiento);
    }

    //Recuperar el valor de un atributo Sexo
    public char getSexo() {
        return sexo;
    }

    //Asignar un valor al atributo Sexo
    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    /**
     * Retorna como Resultado la edad de la persona segun la fecha de nacimiento
     * que el usuario indique.
     */
    private String edad(String fechaNacimiento) {

        int dia = 0;
        int mes = 0;
        int anio = 0;

        String diaArray[] = fechaNacimiento.split("/");

        for (int i = 0; i <= diaArray.length - 1; i++) {
            dia = Integer.parseInt(diaArray[0]);
            mes = Integer.parseInt(diaArray[1]);
            anio = Integer.parseInt(diaArray[2]);
        }

        Period edad = Period.between(LocalDate.of(anio, mes, dia), LocalDate.now());
        String edadactual = String.format("%d:años,%d:meses,%d:días",
                edad.getYears(),
                edad.getMonths(),
                edad.getDays());

        return edadactual;
    }

    //Se define el metodo Abstracto verDatos() de la clase persona.
    abstract public void verDatos();

}
