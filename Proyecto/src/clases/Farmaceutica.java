package clases;

/**
 * Clase Farmaceutica
 * @author Aníbal Roberto Gómez Morales
 * @version 1.0
 * @since 2021-15-08
 */
public class Farmaceutica {

    //Almacena el nombre de la farmaceutica en la variable nombreFarmaceutica.
    private String nombreFarmaceutica;

    /**
     * Brinda valores iniciales a las variables de instancia definidas por la
     * clase Farmaceutica.
     */
    public Farmaceutica(String nombreFarmaceutica) {
        this.nombreFarmaceutica = nombreFarmaceutica;
    }

    //Recuperar el valor de un atributo NombreFarmaceutica.
    public String getNombreFarmaceutica() {
        return nombreFarmaceutica;
    }

    //Asignar un valor al atributo NombreFarmaceutica.
    public void setNombreFarmaceutica(String nombreFarmaceutica) {
        this.nombreFarmaceutica = nombreFarmaceutica;
    }

}
