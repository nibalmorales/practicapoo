/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 * Clase Vacuna
 * @author Aníbal
 */
public class Vacuna extends Farmaceutica {

    /*Almacena un Nombre de la vacuna*/
    public String nombreVacuna;
    /*Almacena un Nombre de la enfermedad que previene*/
    protected String enfermedadQuePreviene;
    /*Almacena la via de administración*/
    protected String viaAdministracion;
    /*Almacena la edad de aplicación*/
    private String EdadAplicacion;
    /*Almacena la dosis y la cantidad*/
    private String dosisYCantidad;

     /**
     * Brinda valores iniciales a las variables de instancia definidas por la
     * clase vacuna.
     */
    public Vacuna(String farmaceutica, String nombreVacuna, String enfermedadQuePreviene, String viaAdministracion, String EdadAplicacion, String dosisYCantidad) {
        super(farmaceutica);
        this.nombreVacuna = nombreVacuna;
        this.enfermedadQuePreviene = enfermedadQuePreviene;
        this.viaAdministracion = viaAdministracion;
        this.EdadAplicacion = EdadAplicacion;
        this.dosisYCantidad = dosisYCantidad;
    }

     /*Recuperar el valor de un atributo NombreVacuna*/
    public String getNombreVacuna() {
        return nombreVacuna;
    }

    /*Asignar un valor al atributo NombreVacuna */
    public void setNombreVacuna(String nombreVacuna) {
        this.nombreVacuna = nombreVacuna;
    }

    /*Recuperar el valor de un atributo EnfermedadQuePreviene*/
    public String getEnfermedadQuePreviene() {
        return enfermedadQuePreviene;
    }

    /*Asignar un valor al atributo EnfermedadQuePreviene*/
    public void setEnfermedadQuePreviene(String enfermedadQuePreviene) {
        this.enfermedadQuePreviene = enfermedadQuePreviene;
    }

     /*Recuperar el valor de un atributo ViaAdministracion*/
    public String getViaAdministracion() {
        return viaAdministracion;
    }

    /*Asignar un valor al atributo ViaAdministracion*/
    public void setViaAdministracion(String viaAdministracion) {
        this.viaAdministracion = viaAdministracion;
    }

      /*Recuperar el valor de un atributo EdadAplicacion*/
    public String getEdadAplicacion() {
        return EdadAplicacion;
    }

    /*Asignar un valor al atributo EdadAplicacion(*/
    public void setEdadAplicacion(String EdadAplicacion) {
        this.EdadAplicacion = EdadAplicacion;
    }

      /*Recuperar el valor de un atributo DosisYCantidad*/
    public String getDosisYCantidad() {
        return dosisYCantidad;
    }

    /*Asignar un valor al atributo DosisYCantidad*/
    public void setDosisYCantidad(String dosisYCantidad) {
        this.dosisYCantidad = dosisYCantidad;
    }

    @Override
    public boolean equals(Object p) {
        return (this.nombreVacuna.equals(((Vacuna) p).nombreVacuna));
    }

}
