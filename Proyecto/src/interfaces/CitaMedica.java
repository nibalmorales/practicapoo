package interfaces;

/**
 *
 * @author Aníbal
 */
public interface CitaMedica {

    public void imprimirCita();

    public void generarReporteVacunacion(String descripcion);

}
